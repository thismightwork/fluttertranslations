import 'package:yaml/yaml.dart';

class Params {
  static const translationsYaml = 'translations';

  String url;
  String projectName;
  String defaultLanguage;
  List<String> languages;

  Params(pubspecContent) {
    final doc = loadYaml(pubspecContent);
    projectName = doc['name'];

    if (projectName == null || projectName.isEmpty) {
      throw Exception(
          'Could not parse the pubspec.yaml, project name not found');
    }

    final config = doc[translationsYaml];
    if (config == null) {
      throw Exception(
          'Could not parse the pubspec.yaml, translations not found or misconfigured');
    }

    final YamlList yamlList = config['languages'];
    if (yamlList == null || yamlList.isEmpty) {
      throw Exception(
          "At least 1 language should be added to the 'languages' section in the pubspec.yaml\n"
          '$translationsYaml'
          "  languages: ['en']");
    }

    languages = yamlList.map((item) => item.toString()).toList();
    if (languages == null || languages.isEmpty) {
      throw Exception(
          "At least 1 language should be added to the 'languages' section in the pubspec.yaml\n"
          '$translationsYaml'
          "  languages: ['en']");
    }

    defaultLanguage = config['default_language'];
    if (defaultLanguage == null) {
      if (languages.contains('en')) {
        defaultLanguage = 'en';
      } else {
        defaultLanguage = languages[0];
      }
    }
    if (!languages.contains(defaultLanguage)) {
      throw Exception('default language is not included in the languages list');
    }

    url = config['url'];
    if (url == null) {
      print(
          "No URL configured, will use local json files instead of fetching from remote.");
    }
  }
}
