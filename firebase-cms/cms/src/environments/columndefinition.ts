import {CollectionDefinition, FieldType, PossibleValueFromCollectionDefinition} from '../app/documents/collection-definition';
import {Document} from '../app/shared/document.model';

export const columnDefinition = new Map<string, CollectionDefinition>(
  [
    ['translations', {
      name: 'translations',
      displayName: 'Translation',
      displayNamePlural: 'Translations',
      collection: 'translations',
      fields: [
        {
          name: 'key',
          displayName: 'Key',
          required: true,
          type: FieldType.Text,
        },
        {
          name: 'nl',
          displayName: 'NL',
          required: true,
          type: FieldType.Text,
        },
        {
          name: 'en',
          displayName: 'EN',
          required: true,
          type: FieldType.Text,
        },
      ],
    } as CollectionDefinition],
  ],
);
