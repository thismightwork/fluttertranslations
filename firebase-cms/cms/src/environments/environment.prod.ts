import {columnDefinition} from './columndefinition';

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "xxx",
    authDomain: "xxx.firebaseapp.com",
    databaseURL: "https://xxx.firebaseio.com",
    projectId: "xxx",
    storageBucket: "xxx.appspot.com",
    messagingSenderId: "xxx",
    appId: "xxx",
    measurementId: "xxx"
  },
  name: 'Flutter Translations',
  columnDefinition
};
