import * as functions from 'firebase-functions';
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const getTranslations = functions.https.onRequest(async (request, response) => {
    const locale = request.query.locale;
    console.log(locale);

    const query = await db.collection('translations').orderBy('key').get();

    let json = "{";

    query.forEach((doc: { data: () => any; }) => {
        const data = doc.data();
        const key = data.key;
        const value = data[locale];
        json += `\"${key}\": \"${value}\",`;
    });
    json = json.substring(0, json.length - 1);
    json += "}";
   
    response.send(json);
})