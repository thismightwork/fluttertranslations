# flutter translations

A dart package to automaticly generate translations dart code.

## Setup

### Add dependency to pubspec

```yaml
translations:
    git:
      url: git@gitlab.com:thismightwork/fluttertranslations.git
      ref: master
```

### Add config to pubspec

Add your locale folder to the assets to make use all your translations are loaded.
```yaml
flutter:
  assets:
    - assets/locale/
```

Add the translations config to fetch the latest translations
```yaml
translations:
  default_language: 'nl'
  languages: ['en', 'nl']
```

### Run package with Flutter

```yaml
flutter packages pub run translations
```

### Arguments

You can pass a String or a num to as an argument.

Formatting for String: %1$s
Formatting for num: %1$d

The number in between % and $ indicate the index of the argument. It is possible to place an argument in 1 language first but in another second:

example:

```
nl '%1$s, ik woon in %2$s. Wist je dat niet?' => Thomas, ik woon in Hasselt. Wist je dat niet?

fr 'I live in %2$s. Didn't you know that, %1$s?" => I live in Hasselt. Didn't you know that, Thomas?
```


# Using the Firebase-hosted CMS for managing Translations
The translations package supports downloading translation .json files from an endpoint which takes a locale as an url parameter (e.g. `https://x.x?locale=en), and return JSON in the body of the response.
For this, a Firebase-hosted CMS is provided which gives you a web-interface for editing the translations, and also provides the JSON endpoint.

## Setting up the CMS

* Install the `Firebase CLI`: https://firebase.google.com/docs/cli

* Create a new Firebase project in the Firebase console https://console.firebase.google.com/, and make sure to enable Web functionality, Firestore, Hosting, and Authentication using Google Accounts. When asked to use the Firebase CLI for setting up the project, use the `firebase-cms` folder as a working directory, but do not deploy any code to Firebase yet.

* When Firebase is set up, go into `firebase-cms/cms/src/environments`, and add your Firebase project details to `environment.ts` and/or `environment.prod.ts`.

* With the `firebase-cms/cms` folder as working directory, run `ng build` or `ng build --prod`. This will create a build of the CMS inside of a `firebase-cms/cms/dist` folder.

* Copy over the contents of `firebase-cms/cms/dist` to `firebase-cms/public`.

* Run `firebase use` to make sure your Firebase CLI instance is linked to the correct Firebase environment, and then run `firebase deploy`.

* Your CMS and Json endpoints should now be live.

## Using the Json Endpoint from the flutter plugin

You should already have the following lines in your project's pubspec.yaml:
```yaml
translations:
  default_language: 'nl'
  languages: ['en', 'nl']
```

Just add the URL endpoint:
```yaml
translations:
  default_language: 'nl'
  languages: ['en', 'nl']
  url: 'https://xxx.cloudfunctions.net/getTranslations'
```

Next time you run the translation tool, it will first fetch the JSON files based on the content of your CMS.
